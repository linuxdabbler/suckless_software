# st

st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.



#### my updated build of st with theming and a few patches

---

This build of st was, at first, forked from Luke Smith, but later built from scratch with alpha, anysize, bold-is-not-bright, delkey, font2, glyph-wide-support, and scrollback. It is set to a gruvbox theme. 

There are several keybindings added to config.h
        * ALT+SHIFT+J = zoom -1
        * ALT+SHIFT+K = zoom +1
        * ALT+SHIFT+D = zoom -2
        * ALT+SHIFT+U = zoom +2
        * ALT+SHIFT+R = zoom reset
        * ALT+SHIFT+C = copy to clipboard
        * ALT+SHIFT+V = paste from clipboard
        
        * Shift+PageUp   = scrollup
        * Shift+PageDown = scrolldown
        
        * Mousebutton2 = paste selection
        * ALT+SHIFT+Mousebutton2 = paste from clipboard
        * Mousebutton4 = scroll up
        * Mousebutton5 = scroll down

---


